import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DashboardAdminRoutingModule } from './dashboard-admin-routing.module';
import { DashboardAdminComponent } from './dashboard-admin.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardComponent } from '../card/card.component';
import { ChartsModule } from 'ng2-charts-x';
import { LyResizingCroppingImageModule } from '@alyle/ui/resizing-cropping-images';
import { AlertModule } from 'ngx-alerts';
import { ResizingCroppingImagesComponent } from 'src/app/shared/resizing-cropping-images/resizing-cropping-images.component';


@NgModule({
  imports: [
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, position: 'right'}),
    LyResizingCroppingImageModule,
    DashboardAdminRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    SharedModule,
    ChartsModule,
    NgbModule.forRoot()
  ],
  exports: [
    TranslateModule
    //ResizingCroppingImagesComponent
  ],
  declarations: [
    DashboardAdminComponent,
    CardComponent,
    ResizingCroppingImagesComponent
   ],
  providers: []
})

export class DashboardAdminModule {}

