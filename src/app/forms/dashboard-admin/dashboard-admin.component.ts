import { Component, OnInit, Input } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from '../../services/language.service'
import { Spanish } from "../../../assets/flatpickr/dist/l10n/es";
import { TranslateService } from '@ngx-translate/core';
import { DashboardAdminService } from 'src/app/services/dashboard-admin.service';
import { NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'ngx-alerts';
import { CroppedImageService } from 'src/app/services/cropped-image.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class DashboardAdminComponent implements OnInit {
  //Changephoto
  pictureForm: FormGroup;
  newPhoto: any;

  nameUser: String;
  descriptionUser: String;

  newNameUser: String;
  newDescriptionUser: String;

  //Calendar 
  model1: Date;
  model2: Date;

  get today() {
    return new Date();
  }

  //Chart
  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];


  //Your Stats
  stats: any;

  //Messages
  sendMessageAdminForm: FormGroup;
  selectedUser: any = {};
  language: string;
  profilePicture = "../../../assets/img/team.jpeg";

  constructor(
    private languageService: LanguageService,
    private translate: TranslateService,
    private dashboardAdminService: DashboardAdminService,
    private modalService: NgbModal,
    private alertService: AlertService,
    private cropService: CroppedImageService) {
    let browserLang: string = translate.getBrowserLang();
    browserLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(browserLang);
    this.language = browserLang;
    localStorage.setItem('language', browserLang);
    this.sendMessageAdminForm = new FormGroup({
      message: new FormControl('', Validators.required),
      sendTo: new FormControl('DCV', Validators.required),
      subject: new FormControl(''),
      user: new FormControl(''),
      picker1: new FormControl('', Validators.required),
      picker2: new FormControl('', Validators.required)
    });
    this.nameUser = "Pompilio";
    this.descriptionUser = "CEO GeoCargo";
    this.pictureForm = new FormGroup({
      purchaseReceipt: new FormControl()
    });

    this.newNameUser = "";
    this.newDescriptionUser = "";
  }


  ngOnInit() {
    this.alertService.success("Inició Sesión Exitosamente")
    this.showStats();
    this.cropService.change.subscribe(cropIMG => {
      this.profilePicture = cropIMG;
    });
  }

  showStats() {
    this.dashboardAdminService.getStats().subscribe(result => {
      if (result['status']) {
        this.stats = result;
      } else {
        console.log("Error");
      }
    });
  }

  sendMessage() {
    //let language = this.languageService.getCurrentLanguage();
    let data = {
      message: this.sendMessageAdminForm.value['message'],
      to_role_name: this.sendMessageAdminForm.value['sendTo'],
      subject: this.sendMessageAdminForm.value['subject'],
      activationDate: this.sendMessageAdminForm.value['picker2'],
      expirationDate: this.sendMessageAdminForm.value['picker'],
      to_user_id: this.selectedUser.id
    }
    if (data.to_role_name == "LUM" && this.selectedUser.id != null) {
      /*this.messageService.sendMessageToUser(data).subscribe((result) => {
        let messageCode = result['code'];
        if (result['status'] === true) {
          this.alertMessageService.showSuccessMessage(language, messageCode);
          this.sendMessageAdminForm.reset();
        }
        else
          this.alertMessageService.showErrorMessage(language, messageCode);
      }, error => {
        const messageCode = 'CE';
        this.alertMessageService.showErrorMessage(language, messageCode);
      });*/
    }
    else {
      /*this.messageService.sendMessageToRol(data).subscribe((result) => {
        let messageCode = result['code'];
        if (result['status'] === true) {
          this.alertMessageService.showSuccessMessage(language, messageCode);
          this.sendMessageAdminForm.reset();
        }
        else
          this.alertMessageService.showErrorMessage(language, messageCode);
      }, error => {
        const messageCode = 'CE';
        this.alertMessageService.showErrorMessage(language, messageCode);
      });*/
    }
  }

  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
    //this.urlBackground = {'background-image':this.getBackgroundURL()};
  }

  openModalEditProfile(modal) {
    this.modalService.open(modal);
  }

  updateProfile() {
    if (this.newNameUser != "") {
      this.nameUser = this.newNameUser;
    }
    if (this.newDescriptionUser != "") {

      this.descriptionUser = this.newDescriptionUser;
    }
    console.log(this.descriptionUser);
    this.alertService.success('Perfil Guardado');
  }
  /* Cmabiar foto de perfil 
  changePhoto() {
    let formData = new FormData();
    formData.append('avatar', this.newPhoto);//--->File

    this.profileService.changeProfilePhoto(formData).subscribe(result => {
      const messageCode = result['code'];
      if (result['status'] === true) {
        this.alertMessageService.showSuccessMessage(language, messageCode);
        let url = result['url'];
        this.avatarURL = url;
        localStorage.setItem('avatarURL', url);
        this.photoName = "";
        this.newPhoto = null;
        this.pictureForm.patchValue({ purchaseReceipt: null });
      }
      else {
        this.alertMessageService.showErrorMessage(language, messageCode);
      }
    }, error => {
      const messageCode = 'CE';
      this.alertMessageService.showErrorMessage(language, messageCode);
    });
  }*/

}
