import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-credit-request',
  templateUrl: './list-credit-request.component.html',
  styleUrls: ['./list-credit-request.component.css']
})
export class ListCreditRequestComponent implements OnInit {
  listReqistrationRequest: any;
  listUsers: any;
  userId: any;
  currentUser: any;
  idAux: any;
  solicitudes: any;


  constructor(
    private translate: TranslateService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar
  ) {
    

   }

  ngOnInit() {
    this.solicitudes= [
      
      {'firstName':'Mariano Renjifo',
      'email': 'mariano@gmail.com', 
      'requesting':'Pablo Dominguez', 
      'monthlyExpenses': '2000000',
      'monthlyIncome': '3000000',
      'requestedAmount':'30000000'},

      {'firstName':'Sebastian Vasquez',
      'email': 'vasquezmejia@gmail.com', 
      'requesting':'Julieta Mendez', 
      'monthlyExpenses': '50000000',
      'monthlyIncome': '30000000',
      'requestedAmount':'150000000'},

      {'firstName':'Marina Granciera',
      'email': 'marianag@gmail.com', 
      'requesting':'Carlos Perez', 
      'monthlyExpenses': '30000000',
      'monthlyIncome': '10000000',
      'requestedAmount':'8000000'},

      {'firstName':'Ricardo Orrego',
      'email': 'ricardorrego@outlook.com', 
      'requesting':'Jaime Bernal', 
      'monthlyExpenses': '3500000',
      'monthlyIncome': '2100000',
      'requestedAmount':'12500000'}
    ];
    
  }





  openModalDetails(modal) {
   
      
      this.modalService.open(modal, { size: 'lg' });
   
  }

  openModalAccept(modal, id) {
    if (id != undefined) {
      this.currentUser = this.getDimensionsByFind(id);
      this.modalService.open(modal, { size: 'lg' });
    } else {
      console.log("Es indefinido")
    }
  }

  openModalDenny(modal, id) {
    if (id != undefined) {
      this.currentUser = this.getDimensionsByFind(id);
      this.modalService.open(modal, { size: 'lg' });
    } else {
      console.log("Es indefinido")
    }
  }

  //Busca un usuario por el id
  getDimensionsByFind(id) {
    console.log(this.listUsers)
    console.log(id);
    return this.listUsers.find(x => x.id === id);
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }

  acceptRequest(){
    console.log(this.currentUser);
    console.log(this.currentUser.id);
   /* this.listReqistrationRequestService.postAcceptRegistratioRequest(this.currentUser.id).subscribe(result => {
      console.log(result)
      if (result['status']) {
        this.openSnackBar("Solicitud Aceptada","");
      } else {
        this.openSnackBar("Error en la solicitud","");
      }
    });*/
  }

  rejectRequest(){
    console.log(this.currentUser);
   /* this.listReqistrationRequestService.postRejectRegistratioRequest(this.currentUser.id).subscribe(result => {
      console.log(this.currentUser.id)
      console.log(result)
      if (result['status']) {
        this.openSnackBar("Solicitud rechazada","");
      } else {
        this.openSnackBar("Error en la solicitud","");
      }
    }); */
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteRow(){

    this.listReqistrationRequest.data.splice(this.userId,1);
    this.listReqistrationRequest._updateChangeSubscription();
  }

}
