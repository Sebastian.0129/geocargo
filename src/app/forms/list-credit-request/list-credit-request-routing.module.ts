import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCreditRequestComponent } from './list-credit-request.component';

const routes: Routes = [
  {
    path: '',
    component: ListCreditRequestComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListCreditRequestRoutingModule { }