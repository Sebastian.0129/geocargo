import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ListCreditRequestRoutingModule } from './list-credit-request-routing.module';
import { ListCreditRequestComponent } from './list-credit-request.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  imports: [
    ListCreditRequestRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    SharedModule,
    NgbModule.forRoot()
  ],
  exports: [
    TranslateModule
  ],
  declarations: [
    ListCreditRequestComponent
  ],
  providers: []
})

export class ListCreditRequestModule {}

