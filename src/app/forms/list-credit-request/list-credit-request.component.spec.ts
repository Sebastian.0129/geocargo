import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCreditRequestComponent } from './list-credit-request.component';

describe('ListCreditRequestComponent', () => {
  let component: ListCreditRequestComponent;
  let fixture: ComponentFixture<ListCreditRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCreditRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCreditRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
