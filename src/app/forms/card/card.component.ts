import { Component, Input, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() cardClass: String;
  @Input() iconClass: String;
  @Input() numberToShow: String;
  @Input() textToShow: String;
  @Input() detailsLink: String;

  constructor(private router:Router) { }

  ngOnInit() {
  }

  redirect(){
    this.router.navigate([this.detailsLink]);
  }
}
