import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LanguageService } from 'src/app/services/language.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  language: string;
  emailForm: FormGroup;
  ruta: string;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private languageService: LanguageService,
    public translate: TranslateService) {
    let browserLang: string = translate.getBrowserLang();
    browserLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(browserLang);
    this.language = browserLang;
    localStorage.setItem('language', browserLang);
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.emailForm = this.formBuilder.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])]
    });
    this.ruta = "../../../assets/img/logo.jpg";
  }

  forgotPassword() {
    console.log("forgotPassword");
  }

  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
    //this.urlBackground = {'background-image':this.getBackgroundURL()};
  }
}
