import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { LanguageService } from 'src/app/services/language.service';
import { sha256 } from 'node_modules/js-sha256';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from 'src/app/services/login.service';
import { AlertService } from 'ngx-alerts';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  language: string;
  urlBackground: any;
  loginForm: FormGroup;
  ruta: string;
  options: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private loginService: LoginService,
    private alert: AlertService,
    private router: Router,
    private languageService: LanguageService,
    public translate: TranslateService) {
    let browserLang: string = translate.getBrowserLang();
    browserLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(browserLang);
    this.language = browserLang;
    localStorage.setItem('language', browserLang);
  }

  ngOnInit() {
    this.validateForm();
  }

  validateForm() {
    this.loginForm = this.formBuilder.group({
      'user_name': ['', Validators.required],
      'password': ['', Validators.required]
    });
    this.ruta = "../assets/img/logo.jpg";
  }

  login() {
    if (this.loginForm.valid) {
      this.loginService.postLogin(this.loginForm.value).subscribe(result => {
        console.log(result);
        if (result == 401) {          
          this.router.navigate(['/login']);
          this.alert.danger("Datos Inválidos")
          this.loginForm.reset();
        } else {
          this.router.navigate(['/dashboard-admin']);
        }

      }, error => {
        this.router.navigate(['/login']);
        this.alert.danger("Datos Inválidos")
        this.loginForm.reset();
      })
    } else {
      this.alert.info("Formulario Inválido")
    }
  }



  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
    //this.urlBackground = {'background-image':this.getBackgroundURL()};
  }
}
