import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AlertModule } from 'ngx-alerts';



@NgModule({
  imports: [
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, position: 'right'}),
    LoginRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    SharedModule,
    NgbModule.forRoot()
  ],
  exports: [
    TranslateModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: []
})

export class LoginModule {}

