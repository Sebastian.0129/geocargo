import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from "../../services/register.service";
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'

  ]

})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  data: any;
  language: string;
  ruta:String;

  constructor(private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private _snackBar: MatSnackBar,
    private translate: TranslateService,
    private languageService: LanguageService) {
    translate.setDefaultLang('es');
    let browserLang: string = translate.getBrowserLang();
    browserLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(browserLang);
    this.language = browserLang;
    localStorage.setItem('language', browserLang);
    this.ruta = "../assets/img/logo.jpg";
  }
  ngOnInit() {
    this.validateForm();
  }

  validateForm() {
    this.registerForm = this.formBuilder.group({
      'social_reason': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'nit': ['', Validators.required]
    });
  }

  register() {
    if (this.registerForm.valid) {
      this.registerService.postRegister(this.registerForm.value).subscribe(result => {
        if (result != null) {
          this.openSnackBar("Solicitud de registro enviada con éxito", "");
        }
        console.log(result);
      }, error => {
        console.log('Datos Erroneos');
      })
    } else {
      this.openSnackBar("Error al enviar la solicitud de registro", "");
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
  }
}

