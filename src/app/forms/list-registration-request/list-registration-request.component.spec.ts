import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRegistrationRequestComponent } from './list-registration-request.component';

describe('ListRegistrationRequestComponent', () => {
  let component: ListRegistrationRequestComponent;
  let fixture: ComponentFixture<ListRegistrationRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRegistrationRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRegistrationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
