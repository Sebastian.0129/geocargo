import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ListRegistrationRequestRoutingModule } from './list-registration-request-routing.module';
import { ListRegistrationRequestComponent } from './list-registration-request.component';
import { MaterialModule } from 'src/app/material.module';
import { AlertModule } from 'ngx-alerts';


@NgModule({
  imports: [
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, position: 'right'}),
    ListRegistrationRequestRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    NgbModule.forRoot(),
    MaterialModule
  ],
  exports: [
    TranslateModule
  ],
  declarations: [
    ListRegistrationRequestComponent
  ],
  providers: []
})

export class ListRegistrationRequestModule {}

