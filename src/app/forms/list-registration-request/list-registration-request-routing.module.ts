import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRegistrationRequestComponent } from './list-registration-request.component';

const routes: Routes = [
  {
    path: '',
    component: ListRegistrationRequestComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListRegistrationRequestRoutingModule { }