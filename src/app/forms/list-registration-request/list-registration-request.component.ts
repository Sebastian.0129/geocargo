import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ListRegistrationRequestService } from 'src/app/services/list-registration-request.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource, MatTabChangeEvent } from '@angular/material';
import { Router } from '@angular/router';
import { LanguageService } from 'src/app/services/language.service';
import { AlertService } from 'ngx-alerts';


@Component({
  selector: 'app-list-registration-request',
  templateUrl: './list-registration-request.component.html',
  styleUrls: ['./list-registration-request.component.css']
})
export class ListRegistrationRequestComponent implements OnInit {
  tableAwaiting: any;
  tableApproved: any;
  tableRejected: any;


  companiesTest = [{
    "id": 1, "social_reason": "Test1", "nit": "123", "email": "test1@gmail.com"
  }, {
    "id": 2, "social_reason": "Test2", "nit": "1234", "email": "test2@gmail.com"
  }, {
    "id": 3, "social_reason": "Test3", "nit": "12345", "email": "test3@gmail.com"
  }]

  tableTest: any;

  listCompaniesAwaiting: any;
  listCompaniesApproved: any;
  listCompaniesRejected: any;
  companyId: any;
  currentCompany: any;
  idAux: any;
  activeTab: Number;

  language: string;

  constructor(private listReqistrationRequestService: ListRegistrationRequestService,
    private changeDetectorRefs: ChangeDetectorRef,
    private translate: TranslateService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private languageService: LanguageService,
    private alertService: AlertService,
    private router: Router, ) {
    let browserLang: string = translate.getBrowserLang();
    browserLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(browserLang);
    this.language = browserLang;
    localStorage.setItem('language', browserLang);
    this.tableTest = new MatTableDataSource(this.companiesTest);

  }

  ngOnInit() {
    this.loadAwaiting();
    this.loadApproved();
    this.loadRejected();
  }

  loadAwaiting() {
    this.listReqistrationRequestService.getAwaiting().subscribe(result => {
      if (result) {
        console.log(result);
        this.listCompaniesAwaiting = result;
        this.tableAwaiting = new MatTableDataSource(this.listCompaniesAwaiting);
        this.changeDetectorRefs.detectChanges();
      } else {
        this.alertService.danger("Error en la solicitud");
      }
    });
  }
  loadApproved() {
    this.listReqistrationRequestService.getApproved().subscribe(result => {
      if (result) {
        this.listCompaniesApproved = result;
        this.tableApproved = new MatTableDataSource(this.listCompaniesApproved);
        this.changeDetectorRefs.detectChanges();
      } else {
        this.alertService.danger("Error en la solicitud");
      }
    });
  }
  loadRejected() {
    this.listReqistrationRequestService.getRejected().subscribe(result => {
      if (result) {
        this.listCompaniesRejected = result;
        this.tableRejected = new MatTableDataSource(this.listCompaniesRejected);
        this.changeDetectorRefs.detectChanges();
      } else {
        this.alertService.danger("Error en la solicitud");
      }
    });
  }

  openModal(modal, id) {
    if (id != undefined) {
      this.currentCompany = this.getDimensionsByFind(id);
      if (this.currentCompany) {
        this.modalService.open(modal, { size: 'lg' });
      } else {
        this.alertService.warning("Compañia es nula ")
      }
    } else {
      this.alertService.warning("El Id Es indefinido")
    }
  }

  //Busca un usuario por el id
  getDimensionsByFind(id) {
    console.log(this.companiesTest);
    console.log(id);
    //return this.companiesTest.find(x => x.id === id);
    if(this.listCompaniesAwaiting.find(x => x.id === id) != null){
      return this.listCompaniesAwaiting.find(x => x.id === id);
    }else if(this.listCompaniesApproved.find(x => x.id === id) != null){
      return this.listCompaniesApproved.find(x => x.id === id);
    }else if(this.listCompaniesRejected.find(x => x.id === id) != null){
      return this.listCompaniesRejected.find(x => x.id === id);
    }
  }

  acceptRequest() {
    this.listReqistrationRequestService.postAcceptRegistratioRequest(this.currentCompany.id).subscribe(result => {
      if (result) {
        this.alertService.success("Solicitud Aceptada exitosamente");
      } else {
        this.alertService.danger("Error en la solicitud");
      }
    }, error => {
      this.alertService.danger("Error en la solicitud");
    });
  }

  rejectRequest() {
    this.listReqistrationRequestService.postRejectRegistratioRequest(this.currentCompany.id).subscribe(result => {
      if (result) {
        this.alertService.info("Solicitud Rechazada exitosamente");
      } else {
        this.alertService.danger("Error en la solicitud");
      }
    }, error => {
      this.alertService.danger("Error en la solicitud");
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteRow() {
    this.tableAwaiting.data.splice(this.companyId, 1);
    this.tableAwaiting._updateChangeSubscription();
  }

  onTabChange(event: MatTabChangeEvent) {
    this.activeTab = event.index;
  }


  editOperator(id) {
    if (id != undefined) {
      this.router.navigate(['/edit-operator/' + id]);
    }
  }

  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
    //this.urlBackground = {'background-image':this.getBackgroundURL()};
  }

  deleteCompany(){
    this.listReqistrationRequestService.postDeleteCompany(this.companyId)
    
  }
}
