
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';


@NgModule({
    imports: [

        NgxMapboxGLModule.withConfig({
            accessToken: 'pk.eyJ1IjoianVhbm1hMjcwMCIsImEiOiJjankwYW93OW0wMTZpM2hwZjA3dW1wOTV4In0.6-MTOmKrtkGL385ut1_Jkw', // Optionnal, can also be set per map (accessToken input of mgl-map)
            geocoderAccessToken: 'pk.eyJ1IjoianVhbm1hMjcwMCIsImEiOiJjankwYW93OW0wMTZpM2hwZjA3dW1wOTV4In0.6-MTOmKrtkGL385ut1_Jkw' // Optionnal, specify if different from the map access token, can also be set per mgl-geocoder (accessToken input of mgl-geocoder)
        }),
        MapRoutingModule,
        CommonModule,
        HttpClientModule,
        TranslateModule,
        SharedModule,
        NgbModule.forRoot()
    ],
    exports: [
        TranslateModule
    ],
    declarations: [
        MapComponent
    ],
    providers: []
})

export class MapModule { }
