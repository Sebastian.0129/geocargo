import { Component, OnInit } from '@angular/core';
import { MapService, NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { map, delay } from 'rxjs/operators';
import * as mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';
import polyline from '@mapbox/polyline';
import * as turf from '@turf/turf';
import { RoutesService } from 'src/app/services/routes.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  // message: 'hello world';


  // markers: any;
  mark: mapboxgl.Marker;


  // style: 'mapbox://styles/mapbox/light-v10',
  style = 'mapbox://styles/mapbox/streets-v11';
  // style: 'mapbox://styles/mapbox/dark-v10',  tema oscuro
  // style: 'mapbox://styles/mapbox/satellite-v9' tema satelital
  lat = 37.75;
  lng = -122.41;
  constructor(private routesService: RoutesService) {
  }

  ngOnInit() {
    this.initializeMap();
  }


  initializeMap() {
    (mapboxgl as typeof mapboxgl).accessToken = environment.mapbox.accessToken;
    var map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      center: [-96, 37.8],
      zoom: 3,
      pitch: 45,
      bearing: -17.6
    });
    let routes = this.routesService.initializeRoutes();
    let markers = [];
    let index = [];
    let sizes = [];
    // Agregar Anmacion a marcador 
    for (let i = 0; i < routes.length; i++) {
      markers[i] = new mapboxgl.Marker();
      sizes[i] = 0;
      index[i] = routes[i].length;
    }
    console.log(index)
    // Agregando polylineas el mapa
    function animateMarker(timestamp) {
      setTimeout(function () {
        // Update the data to a new position based on the animation timestamp. The
        // divisor in the expression `timestamp / 1000` controls the animation speed.
        for (let i = 0; i < markers.length; i++) {          
          markers[i].setLngLat([
            // Math.cos(timestamp / 1000) * radius,
            // Math.sin(timestamp / 1000) * radius
            routes[i][sizes[i]][0], routes[i][sizes[i]][1],
          ]);
          if (sizes[i] === index[i] - 1) {
            sizes[i] = 0;

          } else {
            sizes[i]++;
          }          
          // Ensure it's added to the map. This is safe to call if it's already added.
          markers[i].addTo(map);
        };
        // Request the next frame of the animation.
        requestAnimationFrame(animateMarker);
      }, 200)
    }
    // Start the animation.
    requestAnimationFrame(animateMarker);
    // ----------------------

    // Add map controls
    map.addControl(new mapboxgl.NavigationControl());
    map.addControl(new mapboxgl.FullscreenControl());

    map.addControl(new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      trackUserLocation: true
    }));


    // click en el mapa
    map.on('click', (event) => {
      console.log(JSON.stringify(event.lngLat));
    });


    //Cambiar estilo del mapa
    var layerList = document.getElementById('menu');
    var inputs = layerList.getElementsByTagName('input');

    var colorRoute = {
      '0': "#ffbb00",
      '1': "#00ff11",
      '2': '#ff0000',
      '3': '#ff0000'
    }
    let selectedColor = colorRoute['blue'];

    function switchLayer(layer) {
      var layerId = layer.target.id;
      map.setStyle('mapbox://styles/mapbox/' + layerId);
      if (layerId === 'dark-v10') {
        console.log('cambiando')
        selectedColor = colorRoute['0']
      }
    }
    for (var i = 0; i < inputs.length; i++) {
      inputs[i].onclick = switchLayer;
    }
    // -----------------------


    // Rotar camara

    function rotateCamera(timestamp) {
      // clamp the rotation between 0 -360 degrees
      // Divide timestamp by 100 to slow rotation to ~10 degrees / sec
      map.rotateTo((timestamp / 100) % 360, { duration: 0 });
      // Request the next frame of the animation.
      requestAnimationFrame(rotateCamera);
    }

    map.on('load', function () {
      // rotateCamera(0);
      // //Agregar Zona depeligro
      // map.addLayer({
      //   'id': 'maine',
      //   'type': 'fill',
      //   'source': {
      //     'type': 'geojson',
      //     'data': {
      //       'type': 'Feature',
      //       'geometry': {
      //         'type': 'Polygon',
      //         'coordinates': [[[-75.54511934265764, 5.048031780505369],
      //         [-75.54603945309914, 5.051611232863593],
      //         [-75.54320445521809, 5.055117264807933],
      //         [-75.5391322129338, 5.053359763995431],
      //         [-75.5390134263956, 5.049846226255639],
      //         [-75.54511934265764, 5.048031780505369]]]
      //       }
      //     }
      //   },
      //   'layout': {},
      //   'paint': {
      //     'fill-color': '#ff0000',
      //     'fill-opacity': 0.5
      //   }
      // });
      // -------------------------

      // Vista en 3D 
      var layers = map.getStyle().layers;
      var labelLayerId;
      for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
          labelLayerId = layers[i].id;
          break;
        }
      }

      map.addLayer({
        'id': '3d-buildings',
        'source': 'composite',
        'source-layer': 'building',
        'filter': ['==', 'extrude', 'true'],
        'type': 'fill-extrusion',
        'minzoom': 15,
        'paint': {
          'fill-extrusion-color': '#aaa',

          // use an 'interpolate' expression to add a smooth transition effect to the
          // buildings as the user zooms in
          'fill-extrusion-height': [
            "interpolate", ["linear"], ["zoom"],
            15, 0,
            15.05, ["get", "height"]
          ],
          'fill-extrusion-base': [
            "interpolate", ["linear"], ["zoom"],
            15, 0,
            15.05, ["get", "min_height"]
          ],
          'fill-extrusion-opacity': .6
        }
      }, labelLayerId);


      // Pintar Lineas
      let index = 0
      routes.forEach(route => {
        map.addLayer({
          "id": "route" + index,
          "type": "line",
          "source": {
            "type": "geojson",
            "data": {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "LineString",
                "coordinates": route
              }
            }
          },
          "layout": {
            "line-join": "round",
            "line-cap": "round"
          },
          "paint": {
            "line-color": colorRoute[index],
            "line-width": 8
          }
        });
        index++;
      });
    });
  }

  // getCurrentLocation() {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(position => {
  //       this.lat = position.coords.latitude;
  //       this.lng = position.coords.longitude;
  //       this.map.flyTo({
  //         center: [this.lng, this.lat]

  //       })
  //     })
  //   };
  // }






  drawMap() {
    // var polyline = require('@mapbox/polyline');

    // // returns an array of lat, lon pairs
    // let array = polyline.decode('_p~iF~ps|U_ulLnnqC_mqNvxq`@');

    // returns an array of lat, lon pairs from polyline6 by passing a precision parameter
    // polyline.decode('cxl_cBqwvnS|Dy@ogFyxmAf`IsnA|CjFzCsHluD_k@hi@ljL', 6);

    // // returns a GeoJSON LineString feature
    // polyline.toGeoJSON('_p~iF~ps|U_ulLnnqC_mqNvxq`@');

    // // returns a string-encoded polyline
    // polyline.encode([[38.5, -120.2], [40.7, -120.95], [43.252, -126.453]]);

    // // returns a string-encoded polyline from a GeoJSON LineString
    // polyline.fromGeoJSON({
    //   "type": "Feature",
    //   "geometry": {
    //     "type": "LineString",
    //     "coordinates": [[-120.2, 38.5], [-120.95, 40.7], [-126.453, 43.252]]
    //   },
    //   "properties": {}
    // });
  }

  test() {
  }
}


// Zona de peligro globales

      // map.addSource('earthquakes', {
      //   "type": "geojson",
      //   "data": "https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson"
      // });

      // map.addLayer({
      //   "id": "earthquakes-heat",
      //   "type": "heatmap",
      //   "source": "earthquakes",
      //   "maxzoom": 9,
      //   "paint": {
      //     // Increase the heatmap weight based on frequency and property magnitude
      //     "heatmap-weight": [
      //       "interpolate",
      //       ["linear"],
      //       ["get", "mag"],
      //       0, 0,
      //       6, 1
      //     ],
      //     // Increase the heatmap color weight weight by zoom level
      //     // heatmap-intensity is a multiplier on top of heatmap-weight
      //     "heatmap-intensity": [
      //       "interpolate",
      //       ["linear"],
      //       ["zoom"],
      //       0, 1,
      //       9, 3
      //     ],
      //     // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
      //     // Begin color ramp at 0-stop with a 0-transparancy color
      //     // to create a blur-like effect.
      //     "heatmap-color": [
      //       "interpolate",
      //       ["linear"],
      //       ["heatmap-density"],
      //       0, "rgba(33,102,172,0)",
      //       0.2, "rgb(103,169,207)",
      //       0.4, "rgb(209,229,240)",
      //       0.6, "rgb(253,219,199)",
      //       0.8, "rgb(239,138,98)",
      //       1, "rgb(178,24,43)"
      //     ],
      //     // Adjust the heatmap radius by zoom level
      //     "heatmap-radius": [
      //       "interpolate",
      //       ["linear"],
      //       ["zoom"],
      //       0, 2,
      //       9, 20
      //     ],
      //     // Transition from heatmap to circle layer by zoom level
      //     "heatmap-opacity": [
      //       "interpolate",
      //       ["linear"],
      //       ["zoom"],
      //       7, 1,
      //       9, 0
      //     ],
      //   }
      // }, 'waterway-label');

      // map.addLayer({
      //   "id": "earthquakes-point",
      //   "type": "circle",
      //   "source": "earthquakes",
      //   "minzoom": 7,
      //   "paint": {
      //     // Size circle radius by earthquake magnitude and zoom level
      //     "circle-radius": [
      //       "interpolate",
      //       ["linear"],
      //       ["zoom"],
      //       7, [
      //         "interpolate",
      //         ["linear"],
      //         ["get", "mag"],
      //         1, 1,
      //         6, 4
      //       ],
      //       16, [
      //         "interpolate",
      //         ["linear"],
      //         ["get", "mag"],
      //         1, 5,
      //         6, 50
      //       ]
      //     ],
      //     // Color circle by earthquake magnitude
      //     "circle-color": [
      //       "interpolate",
      //       ["linear"],
      //       ["get", "mag"],
      //       1, "rgba(33,102,172,0)",
      //       2, "rgb(103,169,207)",
      //       3, "rgb(209,229,240)",
      //       4, "rgb(253,219,199)",
      //       5, "rgb(239,138,98)",
      //       6, "rgb(178,24,43)"
      //     ],
      //     "circle-stroke-color": "white",
      //     "circle-stroke-width": 1,
      //     // Transition from heatmap to circle layer by zoom level
      //     "circle-opacity": [
      //       "interpolate",
      //       ["linear"],
      //       ["zoom"],
      //       7, 0,
      //       8, 1
      //     ]
      //   }
      // }, 'waterway-label');

      // // ----------------------