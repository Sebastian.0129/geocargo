import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RequestCreditRoutingModule } from './request-credit-routing.module';
import { RequestCreditComponent } from './request-credit.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  imports: [
    RequestCreditRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    SharedModule,
    NgbModule.forRoot()
  ],
  exports: [
    TranslateModule
  ],
  declarations: [
    RequestCreditComponent
  ],
  providers: []
})

export class RequestCreditModule {}

