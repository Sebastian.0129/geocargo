import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, RequiredValidator } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
import { LanguageService } from '../../services/language.service'
import { TranslateService } from '@ngx-translate/core';
import { RequestCreditService } from 'src/app/services/request-credit.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-request-credit',
  templateUrl: './request-credit.component.html',
  styleUrls: ['./request-credit.component.css']
})
export class RequestCreditComponent implements OnInit {
  creditInfoState = false;
  personalDataState = false;
  laboralDataState = false;
  incomeExpensesState = false;
  foreignOperationsState = false;
  referenceState = false;
  countries = null;
  associateCode: any;
  creditInfo: FormGroup;
  personalData: FormGroup;
  laboralData: FormGroup;
  incomeExpenses: FormGroup;
  foreignOperations: FormGroup;
  familyReference: FormGroup;
  personalReference: FormGroup;
  data: any;
  language: string;
  informationCurrentUser: any;

  constructor(private _formBuilder: FormBuilder,
    private languageService: LanguageService,
    private registerService: RegisterService,
    private translate: TranslateService,
    private requestCreditService: RequestCreditService,
    private _snackBar: MatSnackBar) {
    translate.setDefaultLang('es');
    this.setCountries();
  }

  ngOnInit() {
    this.initializeForms();
    this.laboralData.controls['employeeType'].setValue("Empleado");
  }

  initializeForms() {
    let stringRegularExpression = "([á*é*í*ó*ú*Á*É*Í*Ó*Ú*]*[A-z]*[\\s]*)+";

    this.creditInfo = this._formBuilder.group({
      associatedCode: ['', Validators.required],
      city: [''],
      requestDate: [''],
      loanAmountPesos: [''],
      loanAmountDollar: [''],
      loanDestiny: ['']
    });

    this.personalData = this._formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      typeDoc: [''],
      genre: [''],
      document: [''],
      expeditionDate: [''],
      expeditionPlace: [''],
      bornDate: [''],
      bornCountry: [''],
      bornDepartment: [''],
      bornMunicipality: [''],
      civilStatus: [''],
      childrenNumber: [''],
      address: [''],
      domicileCountry: [''],
      domicileDepartment: [''],
      domicileMunicipality: [''],
      email: [''],
      phone: [''],
      publicResources: ['']
    });

    this.laboralData = this._formBuilder.group({
      employeeType: ['', Validators.required],
      characteristics: ['', Validators.required],
      companyName: [''],
      address: [''],
      phone: [''],
      bornDate: [''],
      position: [''],
      academic: [''],
      universityName: [''],
      careerName: [''],
      semester: ['']
    });

    this.incomeExpenses = this._formBuilder.group({
      monthlyIncome: ['', Validators.required],
      conceptMonthlyIncome: ['', Validators.required],
      monthlyExpenses: [''],
      othersIncome: [''],
      conceptOthersIncome: [''],
      incomeTotal: ['']
    });

    this.foreignOperations = this._formBuilder.group({
      foreignOperationsDesition: ['', Validators.required],
      which: [''],
      foreignAccounts: [''],
      accountNumber: [''],
      bank: [''],
      money: [''],
      country: [''],
      city: ['']
    });

    this.familyReference = this._formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      relationship: [''],
      city: [''],
      address: [''],
      phone: ['']
    });

    this.personalReference = this._formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern(stringRegularExpression)])],
      relationship: [''],
      city: [''],
      address: [''],
      phone: ['']
    });
  }

  setCountries() {
    this.registerService.getCountries().subscribe(result => {
      this.countries = result['countries'];

    }, error => {
      const messageCode = 'CE';
      console.log('error en los countries');
    }
    );
  }

  personalDataValidation() {
    if (this.personalData.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  laboralDataValidation() {
    if (this.laboralData.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  incomeExpensesValidation() {
    if (this.incomeExpenses.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  foreignOperationsValidation() {
    /*
    if(this.foreignOperations.controls['foreignOperationsDesition'].value=='Si'){
      this.foreignOperations.controls['which'].setValidators([Validators.required]);
    }
    else if(this.foreignOperations.controls['foreignOperationsDesition'].value=='No'){
    this.foreignOperations.controls['which'].clearValidators();
    }*/

    if (this.foreignOperations.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  referencesValidation() {
    if (this.familyReference.valid && this.personalReference.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  creditInfoValidation() {
    if (this.creditInfo.valid) {
      return true;
    }
    else {
      return false;
    }
  }

  buttonValidation() {
    if (this.personalDataValidation() && this.laboralDataValidation() && this.incomeExpensesValidation()
      && this.foreignOperationsValidation() && this.referencesValidation() && this.creditInfoValidation()) {
      return true;
    }
    else {
      return false;
    }
  }

  sendData() {
    this.data = {
      "creditInfo": {
        'associatedCode': this.creditInfo.controls['associatedCode'].value,
        'city': this.creditInfo.controls['city'].value,
        'requestDate': this.creditInfo.controls['requestDate'].value,
        'loanAmountPesos': this.creditInfo.controls['loanAmountPesos'].value,
        'loanAmountDollar': this.creditInfo.controls['loanAmountDollar'].value,
        'loanDestiny': this.creditInfo.controls['loanDestiny'].value
      },
      "personalData": {
        'name': this.personalData.controls['name'].value,
        'lastName': this.personalData.controls['lastName'].value,
        'typeDoc': this.personalData.controls['typeDoc'].value,
        'genre': this.personalData.controls['genre'].value,
        'document': this.personalData.controls['document'].value,
        'expeditionDate': this.personalData.controls['expeditionDate'].value,
        'expeditionPlace': this.personalData.controls['expeditionPlace'].value,
        'bornDate': this.personalData.controls['bornDate'].value,
        'bornCountry': this.personalData.controls['bornCountry'].value,
        'bornDepartment': this.personalData.controls['bornDepartment'].value,
        'bornMunicipality': this.personalData.controls['bornMunicipality'].value,
        'civilStatus': this.personalData.controls['civilStatus'].value,
        'childrenNumber': this.personalData.controls['childrenNumber'].value,
        'address': this.personalData.controls['address'].value,
        'domicileCountry': this.personalData.controls['domicileCountry'].value,
        'domicileDepartment': this.personalData.controls['domicileDepartment'].value,
        'domicileMunicipality': this.personalData.controls['domicileMunicipality'].value,
        'email': this.personalData.controls['email'].value,
        'phone': this.personalData.controls['phone'].value,
        'publicResources': this.personalData.controls['publicResources'].value
      },
      "laboralData": {
        'employeeType': this.laboralData.controls['employeeType'].value,
        'characteristics': this.laboralData.controls['characteristics'].value,
        'companyName': this.laboralData.controls['companyName'].value,
        'address': this.laboralData.controls['address'].value,
        'phone': this.laboralData.controls['phone'].value,
        'bornDate': this.laboralData.controls['bornDate'].value,
        'position': this.laboralData.controls['position'].value,
        'academic': this.laboralData.controls['academic'].value,
        'universityName': this.laboralData.controls['universityName'].value,
        'careerName': this.laboralData.controls['careerName'].value,
        'semester': this.laboralData.controls['semester'].value
      },
      "incomeExpenses": {
        'monthlyIncome': this.incomeExpenses.controls['monthlyIncome'].value,
        'conceptMonthlyIncome': this.incomeExpenses.controls['conceptMonthlyIncome'].value,
        'monthlyExpenses': this.incomeExpenses.controls['monthlyExpenses'].value,
        'othersIncome': this.incomeExpenses.controls['othersIncome'].value,
        'conceptOthersIncome': this.incomeExpenses.controls['conceptOthersIncome'].value,
        'incomeTotal': this.incomeExpenses.controls['incomeTotal'].value
      },
      "foreignOperations": {
        'foreignOperationsDesition': this.foreignOperations.controls['foreignOperationsDesition'].value,
        'which': this.foreignOperations.controls['which'].value,
        'foreignAccounts': this.foreignOperations.controls['foreignAccounts'].value,
        'accountNumber': this.foreignOperations.controls['accountNumber'].value,
        'bank': this.foreignOperations.controls['bank'].value,
        'money': this.foreignOperations.controls['money'].value,
        'country': this.foreignOperations.controls['country'].value,
        'city': this.foreignOperations.controls['city'].value
      },
      "familyReference": {
        'name': this.familyReference.controls['name'].value,
        'lastname': this.familyReference.controls['lastname'].value,
        'relationship': this.familyReference.controls['relationship'].value,
        'city': this.familyReference.controls['city'].value,
        'address': this.familyReference.controls['address'].value,
        'phone': this.familyReference.controls['phone'].value
      },
      "personalReference": {
        'name': this.personalReference.controls['name'].value,
        'lastname': this.personalReference.controls['lastname'].value,
        'relationship': this.personalReference.controls['relationship'].value,
        'city': this.personalReference.controls['city'].value,
        'address': this.personalReference.controls['address'].value,
        'phone': this.personalReference.controls['phone'].value
      }
    };
    this.requestCreditService.postSendCreditRequest(this.data).subscribe(result =>{
      console.log(result);
    })
    console.log(this.data);
  }

  getFlagURL(): string {
    const language = this.languageService.getCurrentLanguage();
    return this.languageService.getLanguageFlagURL(language);
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', language);
    //this.urlBackground = {'background-image':this.getBackgroundURL()};
  }

  fillForm() {
    if (this.associateCode) {
      this.requestCreditService.getInformationUser(this.associateCode).subscribe(result => {
        if (result['status']) {
          this.informationCurrentUser = result['user'];
          console.log(this.informationCurrentUser);
        }
        else {
          this.openSnackBar("Código Incorrecto", "UPS")
        }
      })
    } else {
      this.openSnackBar("Código Erroneo", "UPS")
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
