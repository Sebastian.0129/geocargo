import { Injectable, Output, EventEmitter } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class CroppedImageService {

  constructor() { }

  cropIMG = "";
  @Output() change: EventEmitter<string> = new EventEmitter();
  toggle(crop) {
    this.cropIMG = crop;
    this.change.emit(this.cropIMG);
  }
}


