import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RequestCreditService {
  configService: ConfigService
  constructor(private http: HttpClient) {
    this.configService = new ConfigService;
  }

  getInformationUser(userCode: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const params = new HttpParams().set('userCode', userCode);
    return this.http.get(this.configService.serverIp.concat('/getInformationUser'), { headers, params });
  }

  postSendCreditRequest(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.configService.serverIp.concat('/sendCreditRequest'), data, { headers });
  }
}
