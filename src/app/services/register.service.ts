import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams,} from "@angular/common/http";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs/internal/Observable";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class RegisterService {
  configService: ConfigService;

  constructor(public http: HttpClient) {
    this.configService = new ConfigService();
    //////console.log('esta es la ip: ',this.configService);
  }

  postRegister(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.configService.serverIp.concat('/companyRegistration'), data, {headers});
  }

  getCountries():Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.configService.serverIp.concat('/allCountries'), {headers});
  }

 

}

