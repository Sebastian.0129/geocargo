import {Injectable} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})

export class LanguageService {

  constructor(public translate: TranslateService){}

  initLanguageConfig(){
    let language:string = localStorage.getItem('language');
    if(!language){
      language = this.translate.getBrowserLang();
      language = language.match(/en|es/) ? language : 'en';
    }
    this.changeLanguage(language);    
  }

  changeLanguage(language:string){
    return localStorage.getItem('language');
  }

  getLanguageFlagURL(language: string):string{
    let usURL = 'assets/img/flags/us.png';
    let esURL = 'assets/img/flags/es.png';

    if(language == 'es'){
      return esURL;
    }
    if(language == 'en'){
      return usURL;
    }
  }

  getTranslateOf(text: string){
    return this.translate.get(text);
  }

  getCurrentLanguage():string{
    return localStorage.getItem('language');
  }

}
