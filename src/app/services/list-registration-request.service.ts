import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ListRegistrationRequestService {
  configService: ConfigService;

  constructor(public http: HttpClient) {
    this.configService = new ConfigService();
  }

  getAwaiting() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.configService.serverIp.concat('/waitingCompanies'),{headers});
  }

  getApproved() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.configService.serverIp.concat('/approvedCompanies'),{headers});
  }

  getRejected() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.configService.serverIp.concat('/rejectedCompanies'),{headers});
  }

  postAcceptRegistratioRequest(requestId) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.configService.serverIp.concat('/approveCompany'), 
    {'requestId':requestId }, { headers });
  }
  
  postRejectRegistratioRequest(requestId) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.configService.serverIp.concat('/rejectCompany'), {'requestId':requestId }, { headers });
  }

  postDeleteCompany(requestId){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.configService.serverIp.concat('/deleteCompany'), {'requestId':requestId }, { headers });
  }
}
