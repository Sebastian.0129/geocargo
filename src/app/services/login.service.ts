import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';



@Injectable({
  providedIn: 'root'
})
export class LoginService {
  configService: ConfigService;

  constructor(private http: HttpClient) {
    this.configService = new ConfigService();
  }

  postLogin(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    console.log("Conectando ")
    return this.http.post(this.configService.serverIp.concat("/LogIn"), data, { headers });
  }
}
