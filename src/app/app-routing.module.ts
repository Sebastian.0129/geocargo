import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CONTENT_ROUTES} from "./shared/routes/content-layout.routes";
import { AppComponent } from './app.component';
import { Full_ROUTES } from './shared/routes/full-layout.routes';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AppComponent,
    data: {title: 'content Views'},
    children: CONTENT_ROUTES,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
