import { Component, ViewChild, Inject, Renderer2, ElementRef, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { LyTheme2, ThemeVariables } from '@alyle/ui';
import { LyResizingCroppingImages, ImgCropperConfig, ImgCropperEvent } from '@alyle/ui/resizing-cropping-images';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { styles } from './app.styles';
import { CroppedImageService } from 'src/app/services/cropped-image.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'resizing-cropping-images',
  templateUrl: './resizing-cropping-images.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResizingCroppingImagesComponent {
  classes = this.theme.addStyleSheet(styles);
  croppedImg: string;
  @ViewChild(LyResizingCroppingImages) imgCropper: LyResizingCroppingImages;
  scale: number;
  myConfig: ImgCropperConfig = {
    width: 150,
    height: 150,
    fill: '#ff2997'
  };

  constructor(
    private theme: LyTheme2,
    private modalService: NgbModal,
    private cropService: CroppedImageService,
    private alertService: AlertService
  ) {
    this.croppedImg = undefined;
  }
  /** on event */


  onCrop(e: ImgCropperEvent) {
    this.croppedImg = e.dataURL;
    console.log(e);
  }
  /** manual crop */
  getCroppedImg() {
    const img = this.imgCropper.crop();
    console.log(img);
    return img.dataURL;

  }

  openModalChangePhoto(modal) {
    this.modalService.open(modal);
  }

  click() {
    if (this.croppedImg) {
      let browserLang = localStorage.getItem('language');
      this.cropService.toggle(this.croppedImg);
      if (browserLang == "es") {
        this.alertService.success("Foto de perfil actualizada");
      }
      else if (browserLang == "en") {
        this.alertService.success("Profile photo updated");
      }
    }
  }
}