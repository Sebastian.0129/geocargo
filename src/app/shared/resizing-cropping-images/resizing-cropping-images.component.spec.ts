import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResizingCroppingImagesComponent } from './resizing-cropping-images.component';

describe('ResizingCroppingImagesComponent', () => {
  let component: ResizingCroppingImagesComponent;
  let fixture: ComponentFixture<ResizingCroppingImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResizingCroppingImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResizingCroppingImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
