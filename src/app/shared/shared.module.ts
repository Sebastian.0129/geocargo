import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap';
import { IntlTelInputModule } from 'angular-intl-tel-input';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
 imports:      [ 
    CommonModule, 
    FormsModule, 
    MaterialModule, 
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    IntlTelInputModule,
    AngularFontAwesomeModule
],
 exports:      [ 
    CommonModule, 
    FormsModule, 
    MaterialModule, 
    ReactiveFormsModule,
    NgbModule,
    RouterModule
]
})
export class SharedModule { }