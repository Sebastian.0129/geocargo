import {Routes, RouterModule} from '@angular/router';
import {ForgotPasswordComponent} from '../../forms/forgot-password/forgot-password.component';
import {LoginComponent} from '../../forms/login/login.component';
import { RegisterComponent } from 'src/app/forms/register/register.component';
import { RequestCreditComponent } from 'src/app/forms/request-credit/request-credit.component';
import { ListCreditRequestComponent } from 'src/app/forms/list-credit-request/list-credit-request.component';
import { ResizingCroppingImagesComponent } from '../resizing-cropping-images/resizing-cropping-images.component';


export const CONTENT_ROUTES: Routes = [
    {
      path: 'forgot-password',
      loadChildren: './forms/forgot-password/forgot-password.module#ForgotPasswordModule'
    },
    {
      path: 'login',
      loadChildren: './forms/login/login.module#LoginModule'
    },
    {
      path: 'list-registration-request',
      loadChildren: './forms/list-registration-request/list-registration-request.module#ListRegistrationRequestModule'

    },
    {
      path: 'register',
      loadChildren: './forms/register/register.module#RegisterModule'
    },
    {
      path: 'dashboard-admin',
      loadChildren: './forms/dashboard-admin/dashboard-admin.module#DashboardAdminModule'
    },
    {
      path: 'request-credit',
      loadChildren: './forms/request-credit/request-credit.module#RequestCreditModule'
    },
    {
      path: 'list-credit-request',
      loadChildren: './forms/list-credit-request/list-credit-request.module#ListCreditRequestModule'
    },
    {
      path: 'map',
      loadChildren: './forms/map/map.module#MapModule'
    }
  ];